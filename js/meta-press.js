// name   : meta-press.js
// author : Simon Descarpentries, simon /\ acoeuro [] com
// licence: GPLv3
//
// jshint -W097, -W083, -W082, -W033
/* globals provided_newspapers, List, Selectr, crypto, browser */
// crypto : Firefox 26
// fetch : Firefox 39
// includes : Firefox 43
"use strict";
/* * definitions * */
const HEADLINE_PAGE_SIZE = 5;
const MAX_RES_BY_NPP = 10;
const META_FINDINGS_PAGE_SIZE = 15;
const EXCERPT_SIZE = 300;  // charaters
const userLang = navigator.language || navigator.userLanguage;
var newspapers_objs = {};
var newspapers_keys = [];
var current_newspaper_selection = [];
var current_newspaper_nb = 0;
var mp_req = {
	findingList: new List('findings', {  // Definition of a result record via the List.js lib
		item: 'finding-item',
		valueNames: ['f_h1', 'f_txt', 'f_newspaper', 'f_dt', 'f_by',
			{ name: 'f_ISO_dt', attr: 'datetime' },
			{ name: 'f_title_dt', attr: 'title' },
			{ name: 'f_url', attr: 'href' },
			{ name: 'f_icon', attr: 'src' },
			// { name: 'f_selected', attr: 'value' },
		],
		page: 10,
		pagination: { outerWindow: 2, innerWindow: 3 },
		// fuzzySearch: { distance: 1501 }, // Unused. 1000 would search through 400 characters
	}),
	metaFindingsList: new List('meta-findings', { // List of newspaper related info for a query
		item: 'meta-findings-item',
		valueNames: [
			{ name: 'mf_icon', attr: 'src' },
			{ name: 'mf_title', attr: 'title' },
			{ name: 'mf_res_nb', attr: 'data-mf_res_nb' },
			'mf_name',
			'mf_locale_res_nb',
		],
		page: META_FINDINGS_PAGE_SIZE,
		pagination: true
	}),
	running_query_countdown: 0,
	final_result_displayed: 0,
	query_result_timeout: null,
	query_final_result_timeout: null,
	query_start_date: null,
};
document.getElementById('mp_query').addEventListener("keypress", function(evt) {
	if (!evt) evt = window.event;
	var keyCode = evt.keyCode || evt.which;
	if (keyCode == 13) {  // search on pressing Enter key
		document.getElementById('mp_submit').click();
		return false;  // returning false will prevent the event from bubbling up.
	}
});
document.getElementById('mp_submit').addEventListener("click", evt => {
	if (mp_req.running_query_countdown == 0) {  // prevent 2nd query to be launch during a running one
		document.getElementById('mp_submit').disabled = true;
		document.getElementById('waiting_anim').style.display = 'block';
		mp_req.running_query_countdown = current_newspaper_nb;
		// console.log(`Countdown refill ${current_newspaper_nb}`);
		mp_req.query_start_date = new Date();
		clear_results();
		// start_progressbar('findings_progressbar');  // not needed as 1st results appear fast
		const QUERY_STR = document.getElementById('mp_query').value;
		current_newspaper_selection.forEach(i => {
			var n = newspapers_objs[i];
			var npp_start_date = new Date();
			fetch(n.query(QUERY_STR), {
				method: n.method || 'GET',
				headers: { "Content-Type": "application/x-www-form-urlencoded"},
				body: n.body ? n.body(QUERY_STR) : null
			}).then(rep => {
				if (!rep.ok) throw new Error(`fetch not ok but ${rep.status}`);
				var c_type = rep.headers.get("content-type");
				return c_type && c_type.includes("application/json") ? rep.json() : rep.text();
			}).then(rep => {
				if (typeof(rep) == 'string') rep = parse_HTML_from_str(rep);
				if (!n.domain_part) n.domain_part = domain_part(n.headline_url);
				if (!n.favicon_url) n.favicon_url = get_favicon_url(rep, n.domain_part);
				var fdgs = [];
				try{fdgs = n.res(rep)} catch (exc) { console.error(i, rep, exc) }
				var fdgs_nb = fdgs ? fdgs.length : 0;
				var res_nb = 0;
				try{res_nb = n.r_nb(rep)} catch(exc) { console.error(i, rep, exc) }
				res_nb = Number(res_nb) || fdgs_nb;
				var max_fdgs = MAX_RES_BY_NPP;
				fdgs_nb = Math.min(fdgs_nb, max_fdgs);
				if (!fdgs_nb) return search_query_countdown()
				mp_req.metaFindingsList.add({
					mf_icon: n.favicon_url,
					mf_name: i,
					mf_title: `Filter results of "${i}" only`,
					mf_res_nb: res_nb,
					mf_locale_res_nb: res_nb.toLocaleString(),
					// mf_loc_res_nb: fdgs_nb,
					// mf_locale_loc_res_nb: fdgs_nb.toLocaleString(),
				});
				var f_dt;
				for (let f of fdgs) {
					if (max_fdgs-- == 0) break;
					f_dt = new Date(0);
					try {f_dt = parse_dt_str(n.f_dt(f), n.tz, i)} catch (exc) {
						console.error(i, MAX_RES_BY_NPP - max_fdgs, f, exc)}
					try {
						mp_req.findingList.add({
							f_h1:	triw(n.f_h1(f)),
							f_url:	urlify(n.f_url(f), n.domain_part),
							f_icon:	n.favicon_url,
							f_txt:	bolden(shorten(triw(n.f_txt(f)), EXCERPT_SIZE), QUERY_STR),
							f_by:	triw(n.f_by(f)),
							f_dt:	f_dt.toISOString().slice(0, -14),
							f_epoc:	f_dt.valueOf(),
							f_ISO_dt: f_dt.toISOString(),
							f_title_dt: f_dt.toLocaleString(),
							f_newspaper: i,
						});
					} catch (exc) {
						console.error(i, MAX_RES_BY_NPP - max_fdgs, f_dt, exc)
					}
				}
				search_query_countdown();
				console.log(i, new Date() - npp_start_date);
			}).catch(err => { console.error(i, err); search_query_countdown() })
		});
	}
});
function timeoutPromise(ms, promise) {
	// https://stackoverflow.com/questions/37704344/es7-timeout-for-async-await-fetch
	return new Promise((resolve, reject) => {
		const timeoutId = setTimeout(() => { reject(new Error(`timeout (${ms} ms)`)) }, ms);
		promise.then(
			res => { clearTimeout(timeoutId); resolve(res); },
			err => { clearTimeout(timeoutId); reject(err); }
		);
	})
}
function search_query_countdown() {
	mp_req.running_query_countdown -= 1;
	document.getElementById('waiting_anim').style.display = 'none';
	// update_progressbar('findings_progressbar',
	// Math.round(100 - mp_req.running_query_countdown * 100 / current_newspaper_nb));
	clearTimeout(mp_req.query_result_timeout);
	mp_req.query_result_timeout = setTimeout(() => { display_ongoing_results() }, 750);
}
function display_ongoing_results() {
	mp_req.findingList.sort('f_epoc', {order: "desc"});
	mp_req.metaFindingsList.show(0, mp_req.metaFindingsList.size());
	document.querySelectorAll('.mf_name').forEach(a => a.addEventListener("click", evt => {
		mp_req.findingList.search(evt.target.innerText, 'f_newspaper');
		bold_clicked_a(evt.target, '.mf_name');
		mf_date_slicing();
		filter_last(document.getElementById('mf_all_res'), null)
	}));
	mp_req.metaFindingsList.show(0, META_FINDINGS_PAGE_SIZE);
	mp_req.metaFindingsList.sort('mf_res_nb', {order: "desc"});
	document.getElementById('mf_total').addEventListener("click", evt => {
		mp_req.findingList.search();
		bold_clicked_a(evt.target, '.mf_name');
		mf_date_slicing();
		filter_last(document.getElementById('mf_all_res'), null)
	});
	mf_date_slicing();
	for (let sbm of document.querySelectorAll('.sidebar-module')) {
		sbm.style.display='block';
	}
	document.querySelector('footer').style.display='block';
	if (! mp_req.final_result_displayed && mp_req.running_query_countdown == 0)
		display_final_results()
}
function display_final_results() {
	mp_req.final_result_displayed = 1;
	location.href = "#"; location.href = "#mp_query";
	// reset_progressbar('findings_progressbar');
	let mf_res_nb = 0;
	for (let r of mp_req.metaFindingsList.items) mf_res_nb += r.values().mf_res_nb;
	if (0 == mp_req.findingList.size())
		document.getElementById('no_results').style.display = 'block';
	document.getElementById('mp_query_meta_total').innerHTML = mf_res_nb.toLocaleString();
	document.getElementById('mp_query_meta_local_total').innerHTML = mp_req.findingList.size();
	document.getElementById('mp_duration').innerText = (ndt()-mp_req.query_start_date) / 1000;
	document.getElementById('mp_npp_nb').innerText = mp_req.metaFindingsList.items.length;
	document.getElementById('mp_npp_fetched').innerText = current_newspaper_nb;
	document.getElementById('mp_page_sizes').style.display = 'block';
	document.getElementById('mp_submit').disabled = false;
}
function clear_results() {
	mp_req.final_result_displayed = 0;
	document.getElementById('no_results').style.display = 'none';
	document.getElementById('mp_page_sizes').style.display = 'none';
	document.getElementById('mp_query_meta_total').innerHTML = '…';
	document.getElementById('mp_query_meta_local_total').innerHTML = '…';
	document.getElementById('mp_duration').innerText = '…';
	document.getElementById('mp_npp_nb').innerText = '…';
	document.getElementById('mp_npp_fetched').innerText = '…';
	document.getElementById('mp_duration').innerText = '…';
	mp_req.findingList.clear();
	mp_req.metaFindingsList.clear();
	document.getElementById('mp_clear_filter').click()
}
document.getElementById('mp_page_10').addEventListener('click', _ => {mp_req.findingList.show(0, 10)});
document.getElementById('mp_page_50').addEventListener('click', _ => {mp_req.findingList.show(0, 50)});
document.getElementById('mp_page_100').addEventListener('click', _ => {
	mp_req.findingList.show(0, 100)});
document.getElementById('mp_page_all').addEventListener('click', _ => {
	mp_req.findingList.show(0, mp_req.findingList.size())});
document.getElementById('mp_import_JSON').addEventListener('click', evt => {
	let input_elt = document.createElement('input');
	input_elt.type = 'file';
	input_elt.click();
	input_elt.addEventListener("change", evt => {
		mp_req.query_start_date = ndt();
		let file = input_elt.files[0];
		if (file) {
			let reader = new FileReader();
			reader.readAsText(file, "UTF-8");
			reader.onload = function (evt) {
				let input_json = JSON.parse(evt.target.result);
				tags_fromJSON(input_json.filters);
				clear_results();
				mp_req.findingList.add(input_json.findings);
				mp_req.metaFindingsList.add(input_json.meta_findings);
				display_ongoing_results();
				display_final_results();
			}
			reader.onerror = function (evt) { console.error("error reading file to import"); }
		}
	}, false);
});
document.getElementById('mp_export_search_JSON').addEventListener('click', evt => {
	let a = document.createElement('a');
	a.href=`data:application/octet-stream;charset=UTF-8,{
		"filters": ${encodeURIComponent(JSON.stringify(tags_toJSON()))},
		"findings": ${encodeURIComponent(JSON.stringify(mp_req.findingList.toJSON()))},
		"meta_findings": ${encodeURIComponent(JSON.stringify(mp_req.metaFindingsList.toJSON()))}
	}`;
	a.download=`${ndt().toISOString()
			.replace('T', '_')
			.replace(':', 'h')
			.split(':')[0]
		}_meta-press.es.json`;
	a.click();
});
document.getElementById('mp_sel_mod').addEventListener('click', evt => {
	let first_checkbox = document.querySelector('.f_selection');
	let cur_sel_mod = first_checkbox.style.display;
	if (cur_sel_mod == '') cur_sel_mod = 'none';
	let next_sel_mod = cur_sel_mod == 'none' ? 'inline' : 'none';
	document.querySelector('.mp_export_choices').style.display=next_sel_mod;
	for (let a of document.querySelectorAll('#findings li')){
		a.querySelector('.f_selection').style.display=next_sel_mod;
		if (next_sel_mod == 'none') {
			a.querySelector('.f_url').classList.remove('f_sel');
		} else {
			a.querySelector('.f_url').classList.add('f_sel');
		}
	}
});
document.getElementById('mp_export_sel_JSON').addEventListener('click', evt => {
	mp_req.findingList.filter(i => {
		if (i.elm) return i.elm.querySelector('.f_selection').checked});
	let cur_sel_items = [];
	for (let i of mp_req.findingList.matchingItems) cur_sel_items.push(i.values());
	let a = document.createElement('a');
	a.href=`data:application/octet-stream;charset=UTF-8,{
		"filters": ${encodeURIComponent(JSON.stringify(tags_toJSON()))},
		"findings": ${encodeURIComponent(JSON.stringify(cur_sel_items))},
		"meta_findings": ${encodeURIComponent(JSON.stringify(mp_req.metaFindingsList.toJSON()))}
	}`;
	mp_req.findingList.filter();
	a.download=`${ndt().toISOString()
			.replace('T', '_')
			.replace(':', 'h')
			.split(':')[0]
		}_meta-press.es.json`;
	a.click();
});
document.getElementById('mp_export_sel_RSS').addEventListener('click', evt => {
	// http://www.rssboard.org/rss-specification#hrelementsOfLtitemgt
	// https://validator.w3.org/feed/#validate_by_input
	mp_req.findingList.filter(i => {
		if (i.elm) return i.elm.querySelector('.f_selection').checked});
	let flux_items = '';
	let v = {}
	for (let i of mp_req.findingList.matchingItems) {
		v = i.values();
		flux_items += `<item>
            <title>[${v.f_newspaper}] ${v.f_h1}</title>
            <description><![CDATA[${v.f_txt}]]></description>
            <pubDate>${new Date(v.f_ISO_dt).toUTCString()}</pubDate>
            <link>${v.f_url}</link>
			<guid isPermaLink="false">${uuidv4()}</guid>
        </item>`;
	}
	let flux = `<?xml version="1.0" encoding="UTF-8"?>
	<rss version="2.0">
	    <channel>
    	    <title>Meta-Press.es</title>
        	<description>Selection RSS 2.0</description>
    	    <link>https://www.meta-press.es</link>
	        <lastBuildDate>${ndt().toUTCString()}</lastBuildDate>
			<language>${tag_selectrs.lang.getValue()}</language>
    	    <generator>https://www.meta-press.es</generator>
			${flux_items}
		</channel>
	</rss>`;
	let a = document.createElement('a');
	a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(flux)}`;
	mp_req.findingList.filter();
	a.download=`${ndt().toISOString()
			.replace('T', '_')
			.replace(':', 'h')
			.split(':')[0]
		}_meta-press.es.rss`;
	a.click();
});
document.getElementById('mp_export_sel_ATOM').addEventListener('click', evt => {
	// https://tools.ietf.org/html/rfc4287
	// https://validator.w3.org/feed/#validate_by_input
	mp_req.findingList.filter(i => {
		if (i.elm) return i.elm.querySelector('.f_selection').checked});
	let flux_items = '';
	let v = {};
	let item_id = 0;
	for (let i of mp_req.findingList.matchingItems) {
		v = i.values();
		flux_items += `<entry>
            <title>[${v.f_newspaper}] ${v.f_h1}</title>
            <summary type="html"><![CDATA[${v.f_txt}]]></summary>
            <published>${v.f_ISO_dt}</published>
            <updated>${v.f_ISO_dt}</updated>
            <link href="${v.f_url}"/>
			<author><name>${v.f_by}</name></author>
			<id>${v.f_url}</id>
        </entry>`;
		item_id += 1;
	}
	let flux = `<?xml version="1.0" encoding="UTF-8"?>
	<feed xmlns="http://www.w3.org/2005/Atom">
		<title>Meta-Press.es</title>
		<subtitle>Decentralize press search engine</subtitle>
		<link href="https://www.meta-press.es"/>
		<updated>${ndt().toISOString()}</updated>
		<author><name>Meta-Press.es</name></author>
		<id>https://www.meta-press.es/</id>
		${flux_items}
	</feed>`;
	let a = document.createElement('a');
	a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(flux)}`;
	mp_req.findingList.filter();
	a.download=`${ndt().toISOString()
			.replace('T', '_')
			.replace(':', 'h')
			.split(':')[0]
		}_meta-press.es.atom`;
	a.click();
});
function uuidv4() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	)
}
var now = new Date();
function ndt() { return new Date() }
var date_filters = [
	function mf_last_day(r)  {return r.values().f_epoc>ndt().setDate(now.getDate() - 1)},
	function mf_last_2d(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate() - 2)},
	function mf_last_3d(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate() - 3)},
	function mf_last_week(r) {return r.values().f_epoc>ndt().setDate(now.getDate() - 7)},
	function mf_last_2w(r)	 {return r.values().f_epoc>ndt().setDate(now.getDate() - 14)},
	function mf_last_month(r){return r.values().f_epoc>ndt().setMonth(now.getMonth() - 1)},
	function mf_last_2m(r)	 {return r.values().f_epoc>ndt().setMonth(now.getMonth() - 2)},
	function mf_last_6m(r)	 {return r.values().f_epoc>ndt().setMonth(now.getMonth() - 6)},
	function mf_last_year(r) {return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-1)},
	function mf_last_2y(r)	 {return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-2)},
	function mf_last_5y(r)	 {return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-5)},
	function mf_last_10y(r)  {return r.values().f_epoc>ndt().setFullYear(now.getFullYear()-10)}
];
for (let flt of date_filters) document.getElementById(flt.name).addEventListener("click",
	evt => filter_last(evt.target.parentNode.parentNode, flt)
);
document.getElementById('mf_all_res').addEventListener("click",
	evt => filter_last(evt.target.parentNode.parentNode, null)
);
function mf_date_slicing() {
	var prev_nb = 0;
	var total_nb = set_filter_nb(null, 'mf_all_res', 0, 0);
	date_filters.forEach(flt => prev_nb = set_filter_nb(flt, flt.name, prev_nb, total_nb));
}
function set_filter_nb(fct, id, prev_nb, total_nb) {
	mp_req.findingList.filter();
	if(fct) mp_req.findingList.filter(fct);
	var nb = mp_req.findingList.matchingItems.length;
	if (nb > 0 && nb > prev_nb + (total_nb * 0.06)) {
		let p = Math.floor(nb * 30 / mp_req.findingList.size());
		document.getElementById(id+'_nb').innerHTML = nb.toLocaleString()+' '+'.'.repeat(p);
		document.getElementById(id).style.display='block';
	} else {
		document.getElementById(id).style.display='none';
	}
	return nb;
}
function filter_last(target, fct) {
	mp_req.findingList.filter();
	if(fct) mp_req.findingList.filter(fct);
	bold_clicked_a(target, '.mf_dt_filter');
}
function bold_clicked_a(target, a_class) {
	for (let i of document.querySelectorAll(a_class)) { i.classList.remove('bold') }
	target.classList.add('bold');
}
document.getElementById('mp_filter').addEventListener("keyup", searchInResCallback);
document.getElementById('mp_clear_filter').addEventListener("click", function(evt) {
	document.getElementById('mp_filter').value = '';
	searchInResCallback({target: {value: ''}});
});
var searchInResCallback_timeout;
function searchInResCallback (evt) {
	clearTimeout(searchInResCallback_timeout);
	searchInResCallback_timeout = setTimeout(() => {
		if (mp_req.findingList.searched) { document.getElementById('mf_total').click() }
		if (mp_req.findingList.filtered) { filter_last(document.getElementById('mf_all_res'),
			null) }
		if (evt.target.value) {
			mp_req.findingList.search(evt.target.value, ['f_h1', 'f_newspaper', 'f_dt', 'f_by',
				'f_txt'])
		} else {
			mp_req.findingList.search();
		}
		mf_date_slicing()
	}, evt.keyCode == 13 ? 0 : 500);
}
function get_favicon_url(html_fragment, domain_part) {
	var favicon = html_fragment.querySelector('link[rel~="icon"]');  // favicon may be implicit
	return urlify(favicon && favicon.getAttribute("href") || '/favicon.ico', domain_part);
}
function urlify(link, domain_part) {
	if (link.startsWith('http')) {
		return link
	} else {
		if (link.startsWith('file')) {  // remove meta-press.es auto-added path
			return link.replace(document.URL.split('/').slice(0,-1).join('/'), domain_part)
		} else if (link.startsWith('//')) {
			return domain_part + '/' + link.split('/').slice(3).join('/')
		} else {
			return domain_part + (link.startsWith('/') ? '' : '/') + link
		}
	}
}
function parse_dt_str(dt_str, tz, npp_name) {
	var d;
	if (/min(utes?)? ago/i.test(dt_str)) {
		d = timezoned_date('', tz);
		d.setMinutes(d.getMinutes() - Number(dt_str.replace(/min(utes?)? ago/, '')))
	} else if (/hours? ago/i.test(dt_str)) {
		d = timezoned_date('', tz);
		d.setHours(d.getHours() - Number(dt_str.replace(/hours? ago/, '')))
	} else if (/(today|new)/i.test(dt_str)) {
		d = timezoned_date('', tz);
		d.setMinutes(0);
		d.setSeconds(0);
	} else if (/yesterday/i.test(dt_str)) {
		d = timezoned_date('', tz);
		d.setDate(d.getDate() - 1);
		d.setMinutes(0);
		d.setSeconds(0);
	} else {
		d = timezoned_date(dt_str, tz);
	}
	if (!d.getMinutes() && !d.getSeconds()) {  // no time set ? put random one
		d.setHours(Math.min(ndt().getHours(), rnd(1)+rnd(1)+(Math.floor((rnd(1)+1)/2))));
	}
	return d;
}
var intlNum = Intl.NumberFormat('fr', {minimumIntegerDigits: 4, useGrouping: 0});
function timezoned_date (dt_str, tz='UTC') {
	var dt = dt_str ? new Date(dt_str) : new Date();
	if (isNaN(dt)) throw new Error(`${dt_str} is an invalid Date`);
	if (tz == 'UTC' || tz == 'GMT') return dt;
	var dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000);
	var dt_UTC = new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000);
	var dt_repr = dt_UTC.toLocaleTimeString('fr', {timeZoneName: 'short', timeZone: tz});
	var int_offset = parseInt(dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.')*100);
	var tz_offset = intlNum.format(int_offset);
	var tz_repr = int_offset > 0 ? '+'+tz_offset : tz_offset;
	return new Date(dt_orig.toISOString().replace(/\.\d{3}Z/, tz_repr));
}
function bolden(str, search) {
	return str && str.replace(new RegExp(`(${preg_quote(search)})`, 'gi'), "<b>$1</b>");
}
function preg_quote(str) { // http://kevin.vanzonneveld.net, http://magnetiq.com
	// preg_quote("How many? $40"); -> 'How many\? \$40'
	// preg_quote("\\.+*?[^]$(){}=!<>|:"); -> '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'
	/* jshint -W049 */
	str = String(str).replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1")
	/* jshint +W049 */
	return str.replace(/\s/g, '\\s');  // ensure we match converted &nbsp;
}
function shorten(str, at) { return str && str.length > at - 2 ? str.slice(0, at) + '…' : str }
function domain_part(url) { var [htt, , dom] = url.split('/'); return `${htt}//${dom}`; }
function rnd (n) { return Math.ceil (Math.random () * Math.pow (10, n)); }
function parse_HTML_from_str(s) { return new DOMParser().parseFromString(s, "text/html") }
function triw(str) { return str.replace(/(\s\s)*/gi, '').replace(/^\s*|\s*$/gi, '') }
/* * * Headlines * * */
var headlineList = new List('headlines', {
	item: 'headline-item',
	valueNames: [
		'h_title',
		{ name: 'h_url',	attr: 'href' },
		{ name: 'h_icon',	attr: 'src' },
		{ name: 'h_tip',	attr: 'title' },
	],
	page: HEADLINE_PAGE_SIZE,
	pagination: {
		outerWindow: 10,
		innerWindow: 10
	}
});
var headline_item = 1;
var headline_timeout, headline_fadeout_timeout;
function rotate_headlines_timeout() {
	headline_timeout = setTimeout(() => {
		headline_item = (headline_item + HEADLINE_PAGE_SIZE) % headlineList.size();
		headlineList.show(headline_item, HEADLINE_PAGE_SIZE);
		rotate_headlines();
	}, 10000);
}
function rotate_headlines() {
	clearTimeout(headline_fadeout_timeout);
	rotate_headlines_timeout();
	fadeout_pagination_dot('#headlines', 'rgba(0, 208, 192, 1)', 1);
}
function fadeout_pagination_dot(id, bg_color, dot_opacity) {
	headline_fadeout_timeout = setTimeout(() => {
		var dot = document.querySelector(`${id} ul.pagination li.active a.page`);
		dot_opacity -= 0.1;
		dot.style.backgroundColor = bg_color.replace(/, 1\)/, `, ${dot_opacity})`);
		fadeout_pagination_dot(id, bg_color, dot_opacity);
	}, 1000);
}
document.getElementById('headlines').addEventListener("mouseover", () => {
	clearTimeout(headline_timeout);
	clearTimeout(headline_fadeout_timeout);
});
document.getElementById('headlines').addEventListener("mouseout", () => {
	rotate_headlines();
});
function load_headlines() {
	current_newspaper_selection.forEach(i => {
		var n = newspapers_objs[i];
		fetch(n.headline_url/*, {
				method: "GET",
				headers: { overrideMimeType: 'text/plain; charset=x-user-defined' }
		}*/).then(rep => {
			if (!rep.ok) throw `status ${rep.status}`;
			return rep.text();
		}).then(r => {
			r = parse_HTML_from_str(r);
			n.domain_part = domain_part(n.headline_url);
			n.favicon_url = get_favicon_url(r, n.domain_part);
			var h = r.querySelector(n.headline_selector);
			try {
				headlineList.add({
					h_title:n.h_H1 && n.h_H1(h) || h.innerText,
					h_url:	urlify(h.getAttribute('href'), n.domain_part),
					h_icon: n.favicon_url,
					h_tip: `Headline from "${i}" (${ndt().toLocaleString()})`
				});
			} catch (exc) {
				console.error('header for', i, exc);
			}
		}).catch(err => console.error('header for', i, err))
	});
}
/* * * Tags * * */
function populate_tags(newspaper_selection) {
	var available_tags = {
		lang: {},
		country: {},
		themes: {},
		tech: {},
		freq: {}
	};
	newspaper_selection.forEach(k => {
		var n = newspapers_objs[k];
		var lang = n.tags.lang;
		available_tags.lang[lang] = 1 + available_tags.lang[lang] || 1;
		var country = n.tags.country;
		available_tags.country[country] = 1 + available_tags.country[country] || 1;
		var freq = n.tags.freq;
		available_tags.freq[freq] = 1 + available_tags.freq[freq] || 1;
		for (var theme of n.tags.themes) {
			available_tags.themes[theme] = 1 + available_tags.themes[theme] || 1;
		}
		for (var tek of n.tags.tech) {
			available_tags.tech[tek] = 1 + available_tags.tech[tek] || 1;
		}
	});
	for (let tag_name of Object.keys(tag_selectrs)) {
		let s = tag_selectrs[tag_name];
		let s_value = s.getValue();
		s.off('selectr.change', on_tag_sel);
		s.close();
		s.clear();
		s.removeAll();
		for (var t of Object.keys(available_tags[tag_name]).sort())
			s.add({value: t, text:`${t} (${available_tags[tag_name][t]})`});
		s.setValue(s_value);
		s.on('selectr.change', on_tag_sel);
	}
	for (let dns_prefetch_link of document.querySelectorAll('.mp_dns_prefetch')) {
		dns_prefetch_link.outerHTML = '';
	}
	current_newspaper_selection.forEach(i => {
		var n = newspapers_objs[i];
		let l = document.createElement('link');
		l.rel="dns-prefetch";
		l.href=`${n.query('')}`;
		l.classList.add("mp_dns_prefetch");
		document.head.appendChild(l);
	});
}
var selectr_opt = {	clearable: false, placeholder: '…'};
var tag_selectrs = {
	'themes': new Selectr('#tags_themes', Object.assign({}, selectr_opt)),
	'lang': new Selectr('#tags_lang', Object.assign({}, selectr_opt)),
	'country': new Selectr('#tags_country', Object.assign({}, selectr_opt)),
	'freq': new Selectr('#tags_freq', Object.assign({}, selectr_opt)),
	'tech': new Selectr('#tags_tech', Object.assign({}, selectr_opt)),
};
function on_tag_sel() {
	browser.storage.sync.set({filters: tags_toJSON()});
	let new_npp_sel = {};
	for (let tag_name of Object.keys(tag_selectrs)) {
		new_npp_sel[tag_name] = {};
		for (let selected_tag of tag_selectrs[tag_name].getValue()) {
			new_npp_sel[tag_name][selected_tag] = [];
			for (let k of newspapers_keys) {
				if (newspapers_objs[k].tags[tag_name].includes(selected_tag)) {
					new_npp_sel[tag_name][selected_tag].push(k);
				}
			}
		}
	}
	var new_npp_sel_res = [];
	for (let tag_name of Object.keys(tag_selectrs)) { // get the smallest ensemble
		for (let selected_tag of Object.keys(new_npp_sel[tag_name])) {
			if (new_npp_sel_res.length == 0) {
				new_npp_sel_res = new_npp_sel[tag_name][selected_tag]; // init list with smthg
			} else {
				if (new_npp_sel_res.length > new_npp_sel[tag_name][selected_tag].length) {
					new_npp_sel_res = new_npp_sel_res.filter(
						v => new_npp_sel[tag_name][selected_tag].includes(v));
				}
			}
		}
	}
	var new_npp_keys = new_npp_sel_res;
	current_newspaper_selection = new_npp_keys.length == 0 ? newspapers_keys : new_npp_keys;
	current_newspaper_nb = current_newspaper_selection.length;
}
function tags_toJSON() {
	let tags = {};
	for (let t of Object.keys(tag_selectrs)) tags[t] = tag_selectrs[t].getValue();
	return tags;
}
function tags_fromJSON(tag_values) {
	for (let t of Object.keys(tag_values)) {
		tag_selectrs[t].clear();
		tag_selectrs[t].setValue(tag_values[t]);
	}
}
function load_stored_tags(stored_tags) {
	populate_tags(current_newspaper_selection);
	if (typeof(stored_tags) == 'object' && typeof(stored_tags.filters) == 'object') {
		tags_fromJSON(stored_tags.filters);
	}
	if (tag_selectrs.tech.getValue().length == 0) {
		tag_selectrs.tech.setValue(['all', 'https']);
		// tag_selectrs.tech.setValue(['fast']);
	}
	if (tag_selectrs.lang.getValue().length == 0) {
		tag_selectrs.lang.setValue([userLang.slice(0, 2).toLowerCase()]);
	}
}
/* * * end tags * * */
function load_newspapers_objs(stored_data) {
	if (typeof(stored_data) == 'object' && typeof(stored_data.custom_npp) == 'string') {
		var load_from_json = false;
		if (load_from_json) {
			let custom_npp_str = stored_data.custom_npp.replace(/[^:]\/\/.*$/gim, '');
			let custom_npp_obj = JSON.parse(custom_npp_str, (key, value) => {
			if (value && (typeof value === 'string') && (
					(value.indexOf("function") === 0) || (value.indexOf("=>") > 0))) {
					/* jshint -W054 */
					return new Function('return ' + value)();  // revive fct from String
					/* jshint +W054 */
				}
				return value;
			});
		} // else if (load_from_javascript) {
		let custom_npp_str = stored_data.custom_npp;
		let custom_npp_obj = {};
		try {
			/* jshint -W061 */
			custom_npp_obj = eval(custom_npp_str);
			/* jshint +W061 */
		} catch (e) {
			alert(`Could not evaluate your custom newspaper input.\n(${e})`);
		}
		// const new_obj = { ...obj, name: { first: 'blah', last: 'ha'} }
		newspapers_objs = Object.assign(provided_newspapers, custom_npp_obj);
	} else {
		newspapers_objs = provided_newspapers;
	}
	newspapers_keys = Object.keys(newspapers_objs);
	current_newspaper_selection = newspapers_keys;
	current_newspaper_nb = newspapers_keys.length;
	/* * * current_newspaper_selection defined * * */
	for (let k of newspapers_keys) { // Dynamically add the https/http tag on newspapers
		let n = newspapers_objs[k];
		if (n.query('').startsWith('https')) {
			n.tags.tech.push('https');
		} else {
			n.tags.tech.push('http');
		}
	}
	browser.storage.sync.get("filters").then(
		load_stored_tags, err => {console.error(`Failed to retrieve filters: ${err}`);}
	);
	rotate_headlines_timeout();
	load_headlines();
}
/* * end definitions * */
browser.storage.sync.get("custom_npp").then(
	load_newspapers_objs, err => {console.error(`Failed to retrieve custom_npp: ${err}`);}
);
/*(function () {
	document.querySelector('#tooltips_help').;
})();*/
