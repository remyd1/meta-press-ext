;(function (){
	'use strict';
	document.querySelector('#provided_newspapers').innerHTML = js_beautify(
		provided_newspapers.toSource());
		// .replace(/([;{},])([\t ][^"])/g, '$1&#13;&#10;$2')
		// .replace(/(},)/g, '$1&#13;&#10;');
	browser.storage.sync.get("custom_npp").then(
		load_custom_npp, err => {console.error(`Loading settings: ${err}`);}
	);
	function load_custom_npp(stored_data){
		if (typeof(stored_data) == 'object' && typeof(stored_data.custom_npp) == 'string' &&
				stored_data.custom_npp) {
			document.querySelector('#custom_newspapers').innerHTML = stored_data.custom_npp;
			document.querySelector('#reload_hint').style.display = 'inline';
		} else {
			document.querySelector('#custom_newspapers').innerHTML =
				document.querySelector('#default_custom_newspapers').innerHTML;
		}
	}
	document.querySelector('#save_custom_newspapers').addEventListener("click", evt => {
	    browser.storage.sync.set({custom_npp:
				document.querySelector('#custom_newspapers').value});
		document.querySelector('#reload_hint').style.display = 'inline';
		document.querySelector('#reload_hint').style['font-weight'] = 'bold';
	});
}());
