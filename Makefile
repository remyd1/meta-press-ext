zip:
	zip -9 -x *.git* -x js/month_nb/test/* -x *.swp -x *px-sq* -x *LICENSE* -x js/month_nb/READ* -v -r ../`date +%F_%X`-meta-press-ext .

.PHONY: zip
